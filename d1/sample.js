let http = require("http");

//require() is a built in JS method which allows us to import packages
	//packages are pieces of code we can integrate into our application

//http is a default package that comes from NodeJS. It allows us to use methods that let's us create SERVERS

//http is a module. Modules are packages we imported
//Modules are objects that contain codes, methods, or data	

//protocol to client-server communication - http://localhost:5000 - server/applications

http.createServer(function(request,response){

	/*
		createServer() method - is a method from the http module that allows us to handle requests and responses from a client and a server respectively.

		the request object contains details of the request from the clinet

		the response object contains details for the response from the server

		the createServer() method ALWAYS receives the request object first before the response
		
	*/

	/*
		response.writeHead() - is a method of the response object. It allows us to add:

		Headers- Headers are additional information about our response
		We have 2 arguments in our writeHead() method: (1) HTTP status code

		HTTP status is just a numerical code that let the client know about the status of their request

		200 means OK
		404 means resource cannot be found

		(2) "Content-Type" is one of the most recognizable headers. It simply pertains to the data type of our response.


	*/

	/*
		response.end() - ends our response. It is also able to send a message/data as STRING

	*/

	response.writeHead(200,{'Content-Type':'text/plain'});	
	response.end("Hello I'm Erwin Bryan Lansangan");

}).listen(5000);


/* 

	.listen() allows us to assign a port to our server

	This will allow us serve our file (sample.js) server in our local machine assigned to port 5000

	There are several tasks and processes on our computer that run on different port numbers

	hypertext transfer protocol - http://localhost:5000/ - server

	localhost: --- Local Machine : 5000 - current port assigned to our server

	4000, 4040, 8000, 5000, 3000, 4200 - usually used for web development

*/

console.log("Server is running on localhost:5000!");

//we used console.log to show validation from which port number our server is running on.

