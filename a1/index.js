const http = require("http");

//creates a variable "port" to store the port number
const port = 3000

const server = http.createServer((request,response) => {


	if(request.url == '/login'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to the login page.`)
	}


	else if(request.url == '/') {

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`Welcome to the homepage.`)
	}

		
	else{

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 - This page does not exist. Please go back to the homepage.`)
	}

})

server.listen(port);

console.log(`Server is successfully running.:${port}`);

