/*

	What is a client?

	A client an application which creates requests for RESOURCES from a server
	A client will trigger an action, in the web development context, through a URL and wait for the response of the server.


	What is a server?

	A server able to host and deliver RESOURCES requested by a client.
	In fact, a single server can handled multiple clients

	What is Node.JS?

	Node.JS is a runtime environment which allows us to create/develop backend/server-side applications with JavaScript. Because by defauly, JS was conceptualized solely to the front-end.
	Runtime Environment - is the environment in which a program or application is executed

	Why is Node.JS Popular?

	Performance - NodeJS is on the most performing environment for creating backend applications with JavaScript

	Familiarity - Since NodeJS is build and uses JS as its language, it is very familiar with most developers

	NPM - Node Package Manager - is the largest registry for node packages
	Packages - are bits of programs, methods, functions, codes that greatly help in the development of an application

*/


const http = require("http");

//creates a variable "port" to store the port number
const port = 4000

const server = http.createServer((request,response) => {


	if(request.url == '/greeting'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to 248 2048 App`)
	}

	else if (request.url == '/homepage')
		{

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`This is the homepage!`)
	}

	//create an else condition that all other routes will return a message of "Page Not Available"

		
	else{

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 - Page Not available.`)
	}

})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);

